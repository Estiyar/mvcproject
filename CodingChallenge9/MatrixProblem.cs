﻿using System;

namespace CodingChallenge9
{
    class MatrixProblem
    {
        /// <summary>
        /// This was edited by manoj
        /// edited by dhakshan
        /// 1.Adding elemnts of matrix - Manoj
        /// 2.print only left digonal elements - Done by Estiyar Khan
        /// 3.print 1st and last row - Dakshan
        /// 4.print 1st and last col - Gopal
        /// 5.print only right digonal elements. - Ajinkya
        /// 6.print Right Triangle-Bhumika
        /// </summary>

        static void Main(string[] args)
        {
            Console.Write("Enter the row value: ");
            int row = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Enter the column value: ");
            int col = Convert.ToInt32(Console.ReadLine());

            int[,] matrix = new int[row, col];

            Console.WriteLine("Enter element of matrix: ");

            for (int i = 0; i < row; i++) // it will check the row values.
            {
                for (int j = 0; j < col; j++) //it will check the column values.
                {
                    Console.Write("enter element at index-[{0},{1}]:",i,j);
                    matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            /*2.Adding the elements of matrix--Done by Manoj*/
            AddElementsOfMatrix(matrix, row, col);

            InterChangeMatrix(matrix, row, col);
            Diplay(matrix, row, col);
            //4. print rocolumn by GopalKrishna
            PrintColumn(matrix, row, col);

            /*1. Print digonal elents - Done by Estiyar Khan*/
            PrintPrincipleElemts(matrix, row, col);
            Console.ReadKey();

            /*3.Print 1st and last row--Done by Dhakshan*/
            PrintRows(matrix, row, col);

            /*4.Print Right Triangle -----done by bhumika*/
            LeftTriangle(matrix, row, col);
        }

        private static void LeftTriangle(int[,] matrix, int row, int col)
        {
            Console.WriteLine("Top triangle is: ");
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (i >= j)
                    {
                        Console.Write("0" + " ");
                    }
                    else
                    {
                        Console.Write(matrix[i, j] + " ");
                    }

                }
                Console.WriteLine();
            }
            Console.ReadLine();

        }



        private static void DisplayRightAngledTriangle(int[,] matrix, int row, int col)
        {
            int size = CountElement(matrix, row, col);
            int[] temp = new int[size];
            int index = 0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j <=i; j++)
                {
                    temp[index++] = matrix[i, j];
                }
            }

            for (int k = 0; k < temp.Length; k++)
            {
                Console.WriteLine(temp[k]);
            }

        }

        private static int CountElement(int[,] matrix, int row, int col)
        {
            int size = 0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j <=i; j++)
                {
                    size++;
                }

            }
            return size;
        }

        //Adding elements of matrix
        private static void AddElementsOfMatrix(int[,] samplematrix, int rows, int column)
        {
            int result = 0;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    result += samplematrix[i, j];
                }
            }
            Console.WriteLine("sum of matrix elements is : " + result);
        }


        //Print digonal elements
        private static void Diplay(int[,] sampleMatrix2, int rows, int column)
        {
            Console.WriteLine("The value of matrix: ");

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    Console.Write(sampleMatrix2[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        /*1. Print digonal elents - Done by Estiyar Khan*/
        private static void PrintPrincipleElemts(int[,] ematrix, int erow, int ecol)
        {
            Console.WriteLine("The Principle digonal value of matrix: ");

            for (int i = 0; i < erow; i++)
            {
                for (int j = 0; j < ecol; j++)
                {
                    // Condition for principal diagonal
                    if (i == j)
                        Console.Write(ematrix[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        private static int[,] InterChangeMatrix(int[,] matrix, int rows, int cols)
        {
            Console.WriteLine("Enter first value: ");
            int first = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter second value: ");
            int second = int.Parse(Console.ReadLine());

            for (int i = 0; i < rows; i++)
            {
                int temp = matrix[i, first - 1] * matrix[i, first - 1];
                matrix[i, first - 1] = matrix[i, second - 1] * matrix[i, second - 1];
                matrix[i, second - 1] = temp;
            }

            return matrix;
        }
        /*3. Print first and last rows - Done by Dhashnamoorthy*/
        private static void PrintRows(int[,] matrix, int row, int col)
        {
            Console.WriteLine("Display first and last rows");
            for(int i=0;i<row;i++)
            {
                for(int j=0;j<col;j++)
                {
                    if(i==0||i==row-1)
                    {
                        Console.WriteLine(matrix[i,j]+" ");
                    }
                }
                Console.WriteLine();
            }
        }
        //Method Created By GopalKrishna
        private static void PrintColumn(int[,] matrix, int row, int col)
        {
            Console.WriteLine("Display first and last Columns");
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (j == 0 || j == col - 1)
                    {
                        Console.Write(matrix[i, j] + " ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}