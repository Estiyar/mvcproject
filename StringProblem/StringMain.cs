﻿using System;

namespace StringProblem
{
    class StringMain
    {
        /// <summary>
        /// 1. Reverse string - Sama
        /// 2.Concat string - Hari
        /// 3.replace string - Manideep
        /// 4. reverse sentance of string - Sandhya
        /// 5. Check vowel in string - Shubhashani
        /// 
        /// </summary>
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first String:");
            string firstValue = Console.ReadLine();

            Console.WriteLine("Enter second String:");
            string secondValue = Console.ReadLine();

            string result = FindCommonString(firstValue, secondValue);

            string concat = ConcatStrings(firstValue, secondValue);

            Console.WriteLine("concat string:"+ concat);

            Console.WriteLine("String:" + result);

            ReversSentence();

            Console.ReadLine();
        }

        private static string ConcatStrings(string firstValue, string secondValue)
        {
            string result = firstValue + secondValue;
            return result;
        }

        private static string FindCommonString(string firstValue, string secondValue)
        {
            int count = 0;
            for (int i = 0; i < firstValue.Length; i++)
            {
                if (firstValue[i] == ' ')
                {
                    count++;
                }
            }
            string[] sentanceOne = new string[count + 1];
            int index = 0;
            string temp = "";

            for (int i = 0; i < firstValue.Length; i++)
            {
                if (firstValue[i] == ' ')
                {
                    sentanceOne[index] = temp;
                    index++;
                    temp = "";
                }
                else
                {
                    temp += firstValue[i];
                }
            }
            sentanceOne[index] = temp;

            int counts = 0;

            for (int i = 0; i < secondValue.Length; i++)
            {
                if (secondValue[i] == ' ')
                {
                    counts++;
                }
            }

            string[] sentanceTwo = new string[counts + 1];
            int indexs = 0;
            string value = "";

            for (int i = 0; i < secondValue.Length; i++)
            {
                if (secondValue[i] == ' ')
                {
                    sentanceTwo[indexs] = value;
                    indexs++;
                    value = "";
                }
                else
                {
                    value += secondValue[i];
                }
            }

            sentanceTwo[indexs] = value;

            string sentance = "";

            for (int i = 0; i < sentanceOne.Length; i++)
            {
                string word = sentanceOne[i];
                for (int j = 0; j < sentanceTwo.Length; j++)
                {
                    if (word.CompareTo(sentanceTwo[j]) == 0)
                    {
                        sentance += word;
                    }
                }
                sentance += " ";
            }

            return sentance;
        }
        //reverse of the sentence by sandhya
        public static void ReversSentence()
        {

            string str, RevStr = "";
            Console.WriteLine("Enter a sentence  to reverse : ");
            str = Console.ReadLine();
            for (int x = str.Length - 1; x >= 0; x--)
            {
                RevStr += str[x];
            }
            Console.WriteLine("Reverse string is : {0}", RevStr);
            Console.ReadLine();

        }
    }
 }

